# ninjapants-spinnaker-repo

```bash
terraform plan \
  -var-file="secret.tfvars" \
  -var-file="production.tfvars"
terraform apply \
  -var-file="secret.tfvars" \
  -var-file="production.tfvars"
gcloud container clusters get-credentials <cluster_name> --region <region>
```