#####################################################################
# Variables
#####################################################################
variable "username" {
  default = "admin"
}

variable "password" {}
variable "project" {}
variable "region" {}
variable "clustername" {}


#####################################################################
# Modules
#####################################################################
module "gke" {
  source      = "./gke"
  project     = "${var.project}"
  region      = "${var.region}"
  username    = "${var.username}"
  password    = "${var.password}"
  clustername = "${var.clustername}"
}

