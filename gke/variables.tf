#####################################################################
# Variables
#####################################################################
variable "project" {}
variable "clustername" {}
variable "region" {}

variable "username" {
  default = "admin"
}

variable "password" {}

variable "np-scope" {
  default = [
               "storage-ro",
               "logging-write",
               "monitoring-write",
               "service-management",
               "service-control",
               "trace-append",
               "compute-rw"
            ]
  description = "scope for GKE node pool"
}
