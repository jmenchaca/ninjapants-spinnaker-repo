#####################################################################
# GKE Cluster
#####################################################################
resource "google_container_node_pool" "np" {
  name       = "${var.clustername}-node-pool"
  region     = "${var.region}"
  cluster    = "${google_container_cluster.primus.name}"
  node_count = 1

  node_config {
    preemptible  = false
    machine_type = "n1-standard-2"
    oauth_scopes = "${var.np-scope}"
  }
}

resource "google_container_cluster" "primus" {
  name               = "${var.clustername}"
  region             = "${var.region}"

  addons_config {
    network_policy_config {
      disabled = true
    }
  }

  master_auth {
    username = "${var.username}"
    password = "${var.password}"
  }
    lifecycle {
    ignore_changes = ["node_pool"]
  }

  node_pool {
    name = "default-pool"
  }
}

#####################################################################
# Output for K8S
#####################################################################
output "client_certificate" {
  value     = "${google_container_cluster.primus.master_auth.0.client_certificate}"
  sensitive = true
}

output "client_key" {
  value     = "${google_container_cluster.primus.master_auth.0.client_key}"
  sensitive = true
}

output "cluster_ca_certificate" {
  value     = "${google_container_cluster.primus.master_auth.0.cluster_ca_certificate}"
  sensitive = true
}

output "host" {
  value     = "${google_container_cluster.primus.endpoint}"
  sensitive = true
}
